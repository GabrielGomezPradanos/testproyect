package com.calendar.sp.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;

/**
 * This class contains the Event data
 * 
 * @author anunez
 * 
 */
@XmlRootElement
public class ExceptionBean implements Serializable {

	private static final long serialVersionUID = 1532936431829837736L;

	@Expose
	private String code;

	@Expose
	private String message;

	public ExceptionBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ExceptionBean(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}


	public String getMessage() {
		return message;
	}


}
