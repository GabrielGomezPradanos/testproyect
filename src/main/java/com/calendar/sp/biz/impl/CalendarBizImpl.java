package com.calendar.sp.biz.impl;

import java.util.HashMap;
import java.util.logging.Logger;

import com.calendar.sp.entities.EventEntity;
import com.calendar.sp.exceptions.CalendarException;

public class CalendarBizImpl{

	public static final int ERROR_CONSTANT = 6008;
	private static final Logger LOGGER = Logger.getLogger(CalendarBizImpl.class.getName());
	public final static String CALENDAR_ID_EMPTY_ERROR = "The field: calendarId can not be null or empty.";
	public final static String START_DATE_TIME_EMPTY_ERROR = "The field: startDateTime can not be null or empty.";
	public final static String END_DATE_TIME_EMPTY_ERROR = "The field: endDateTime can not be null or empty.";
	public final static String ATTENDEES_EMAIL_EMPTY_ERROR = "The field: attendeesEmail can not be null or empty.";
	public static final String EVENT_ID_EMPTY_ERROR = "The field: eventId can not be null or empty.";

	public EventEntity createEvent(String calendarId, final String startDateTime, final String endDateTime,
			String attendeesEmail, final String description, final Boolean guestsCanInviteOthers,
			final Boolean guestsCanSeeOtherGuests, final String location, final String summary, final String colorId,
			HashMap<String, String> extendedProperties, Boolean sendNotifications, Boolean moveService,
			final Boolean classification) {

		if ((calendarId == null) || calendarId.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR,
					ERROR_CONSTANT);
		} else {
			calendarId = calendarId.toLowerCase();
		}
		if ((startDateTime == null) || startDateTime.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR,
					ERROR_CONSTANT);
		}
		if ((endDateTime == null) || endDateTime.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR,
					ERROR_CONSTANT);
		}
		if ((attendeesEmail == null) || attendeesEmail.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR,
					ERROR_CONSTANT);
		}

		EventEntity eventEntity = new EventEntity(startDateTime, endDateTime, attendeesEmail, description, guestsCanInviteOthers,
						guestsCanSeeOtherGuests, location, summary, colorId, extendedProperties);

		return eventEntity;
	}

	public EventEntity updateEvent(String calendarId, String startDateTime, String endDateTime, String attendeesEmail,
			String description, Boolean guestsCanInviteOthers, Boolean guestsCanSeeOtherGuests, String location,
			String summary, String colorId, String eventId, HashMap<String, String> extendedProperties,
			Boolean sendNotifications, final Boolean classification) {

		if ((calendarId == null) || calendarId.isEmpty()) {
			throw new CalendarException(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR,
					ERROR_CONSTANT);
		} else {
			calendarId = calendarId.toLowerCase();
		}
		if ((eventId == null) || eventId.isEmpty()) {
			throw new CalendarException(CalendarBizImpl.EVENT_ID_EMPTY_ERROR,
					ERROR_CONSTANT);
		}
		if ((startDateTime == null) || startDateTime.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR,
					ERROR_CONSTANT);
		}
		if ((endDateTime == null) || endDateTime.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR,
					ERROR_CONSTANT);
		}
		if ((attendeesEmail == null) || attendeesEmail.isEmpty()) {
			CalendarBizImpl.LOGGER.warning(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR);
			throw new CalendarException(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR,
					ERROR_CONSTANT);
		}

		EventEntity eventEntity = new EventEntity(eventId, startDateTime, endDateTime, attendeesEmail, description, guestsCanInviteOthers,
						guestsCanSeeOtherGuests, location, summary, colorId, extendedProperties);

		return eventEntity;
	}
}
