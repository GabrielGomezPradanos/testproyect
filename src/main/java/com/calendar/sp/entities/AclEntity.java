package com.calendar.sp.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AclEntity implements Serializable {

	//asdfasdf
	
	private static final long serialVersionUID = 9084036015124704355L;
	private String etag;
	private String kind;
	private String nextPageToken;
	private List<AclRuleEntity> items;

	public AclEntity() {
		items =new ArrayList<AclRuleEntity>();
	}

	public AclEntity(String etag, String kind, String nextPageToken,
			List<AclRuleEntity> items) {
		this.etag = etag;
		this.kind = kind;
		this.nextPageToken = nextPageToken;
		this.items = items;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getNextPageToken() {
		return nextPageToken;
	}

	public void setNextPageToken(String nextPageToken) {
		this.nextPageToken = nextPageToken;
	}

	public List<AclRuleEntity> getItems() {
		return items;
	}

	public void setItems(List<AclRuleEntity> items) {
		this.items = items;
	}
	
	public void addItem(AclRuleEntity item) {
		if(this.items==null){
			this.items = new ArrayList<AclRuleEntity>();
		}
		this.items.add(item);
	}

}
