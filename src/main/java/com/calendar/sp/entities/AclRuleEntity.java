package com.calendar.sp.entities;

import java.io.Serializable;

public class AclRuleEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6992433436350555762L;
	private String id;
	private String etag;
	private String kind;
	private String role;
	private ScopeEntity scope;

	public AclRuleEntity() {
	}

	public AclRuleEntity(String id, String etag, String kind, String role,
			ScopeEntity scope) {
		this.id = id;
		this.etag = etag;
		this.kind = kind;
		this.role = role;
		this.scope = scope;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEtag() {
		return etag;
	}

	public void setEtag(String etag) {
		this.etag = etag;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public ScopeEntity getScope() {
		return scope;
	}

	public void setScope(ScopeEntity scope) {
		this.scope = scope;
	}

}
