package com.calendar.sp.entities;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.googlecode.objectify.annotation.Embed;

/**
 * This class contains the data of an event attendee
 *
 * @author anunez
 *
 */

@Embed
public class AttendeeEntity implements Serializable {
	private static final long serialVersionUID = 7412881263728741686L;

	@Expose
	private String comment;

	/**
	 * The attendee's name, if available. Optional. The value may be
	 * {@code null}.
	 */
	@Expose
	private String displayName;

	/**
	 * Whether this entry represents the calendar on which this copy of the
	 * event appears. Read-only. The default is False. The value may be
	 * {@code null}.
	 */
	@Expose
	private Boolean self;

	/**
	 * The attendee's response status. Possible values are: - "needsAction" -
	 * The attendee has not responded to the invitation. - "declined" - The
	 * attendee has declined the invitation. - "tentative" - The attendee has
	 * tentatively accepted the invitation. - "accepted" - The attendee has
	 * accepted the invitation. The value may be {@code null}.
	 */
	@Expose
	private String responseStatus;

	/**
	 * Number of additional guests. Optional. The default is 0. The value may be
	 * {@code null}.
	 */
	@Expose
	private Integer additionalGuests;

	/**
	 * Whether the attendee is a resource. Read-only. The default is False. The
	 * value may be {@code null}.
	 */
	@Expose
	private Boolean resource;

	/**
	 * Whether the attendee is the organizer of the event. Read-only. The
	 * default is False. The value may be {@code null}.
	 */
	@Expose
	private Boolean organizer;

	/**
	 * Whether this is an optional attendee. Optional. The default is False. The
	 * value may be {@code null}.
	 */
	@Expose
	private Boolean optional;

	/**
	 * The attendee's email address, if available. This field must be present
	 * when adding an attendee. The value may be {@code null}.
	 */
	@Expose
	private String email;

	@Expose
	private String id;


	public AttendeeEntity(String email, String responseStatus) {
		super();
		this.email = email;
		this.responseStatus = responseStatus;
	}

	public String getEmail() {
		return email;
	}

}
