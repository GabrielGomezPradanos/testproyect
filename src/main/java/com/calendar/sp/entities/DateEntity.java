package com.calendar.sp.entities;

import java.io.Serializable;

import com.calendar.sp.utils.DateUtils;
import com.google.gson.annotations.Expose;
import com.googlecode.objectify.annotation.Embed;

/**
 * This class contains the date`s values
 *
 * @author anunez
 *
 */

@Embed
public class DateEntity implements Serializable {

	private static final long serialVersionUID = 4745773797496757125L;

	/**
	 * Date/time value expressed as the number of ms since the Unix epoch.
	 */
	@Expose
	private long value;

	/**
	 * Time zone shift from UTC in minutes. If {@code null}, no time zone is
	 * set, and the time is always interpreted as local time.
	 */
	@Expose
	private Integer tmzShift;

	/**
	 * DateTime string in format RFC 3339
	 */
	@Expose
	private String dateTime;

	/**
	 * the name of the timezone
	 */
	@Expose
	private String timezone;

	/**
	 * Date in format 'yyyy-MM-dd'
	 */
	private String date;

	public DateEntity() {

	}

	public DateEntity(long value, Integer tmzShift) {
		super();
		this.value = value;
		this.tmzShift = tmzShift;
	}

	public void setDateTime(String data) throws NumberFormatException {
		dateTime = data;
		if (dateTime != null) {
			DateEntity auxDate = DateUtils.valueInRFCToDateEntity(data);
			value = auxDate.getValue();
			timezone = auxDate.getTimezone();
			tmzShift = auxDate.getTmzShift();
		}
	}

	public long getValue() {
		return value;
	}

	public Integer getTmzShift() {
		return tmzShift;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public void setTmzShift(Integer tmzShift) {
		this.tmzShift = tmzShift;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

}
