package com.calendar.sp.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * This class contains the Event data
 *
 * @author anunez
 *
 */
public class EventEntity implements Serializable {
	private static final long serialVersionUID = 6168602347146575399L;

	@Expose
	private DateEntity created;
	@Expose
	private String creator;
	@Expose
	private String etag;
	@Expose
	private String htmlLink;
	@Expose
	private String iCalUID;
	@Expose
	private String id;
	@Expose
	private String kind;
	@Expose
	private String organizer;
	@Expose
	private ReminderEntityList reminders;
	@Expose
	private Integer sequence;
	@Expose
	private String status;
	@Expose
	private String summary;
	@Expose
	private DateEntity updated;
	@Expose
	private String location;
	@Expose
	private List<AttendeeEntity> attendees = new ArrayList<AttendeeEntity>();
	@Expose
	private String visibility;
	@Expose
	private String transparency;
	@Expose
	private String description;
	@Expose
	private ExtendedPropertiesEntity extendedProperties;
	@Expose
	private String colorId;
	@Expose
	private Boolean guestsCanInviteOthers;
	@Expose
	private Boolean guestsCanSeeOtherGuests;
	@Expose
	private Boolean guestsCanModify;
	@Expose
	private DateEntity startDate;
	@Expose
	private DateEntity endDate;
	@Expose
	private DateEntity originalStartTime;
	@Expose
	private java.util.List<String> recurrence;
	@Expose
	private String recurringEventId;
	@Expose
	private String hangoutLink;

	public EventEntity(String startDateTime, String endDateTime, String attendeesEmail, String description,
			Boolean guestsCanInviteOthers, Boolean guestsCanSeeOtherGuests, String location, String summary,
			String colorId, HashMap<String, String> extendedProperties) {

		this.summary = summary;
		DateEntity startDate = new DateEntity();
		startDate.setDateTime(startDateTime);
		DateEntity endDate = new DateEntity();
		endDate.setDateTime(endDateTime);
		this.startDate = startDate;
		this.endDate = endDate;
		for (String emailAttendee : attendeesEmail.split(",")) {
			attendees.add(new AttendeeEntity(emailAttendee.trim(), "accepted"));
		}
		this.description = description;
		this.guestsCanInviteOthers = guestsCanInviteOthers;
		this.guestsCanSeeOtherGuests = guestsCanSeeOtherGuests;
		this.location = location;
		this.summary = summary;
		this.colorId = colorId;
		this.extendedProperties = new ExtendedPropertiesEntity();
		if (extendedProperties == null) {
			extendedProperties = new HashMap<String, String>();
		}
		extendedProperties.put("appId", "testProject");
		this.extendedProperties.setPrivateDataStore(extendedProperties);

		// Managing Reminders
		ReminderEntity reminderEntityPopup = new ReminderEntity();
		reminderEntityPopup.setMinutes(10);
		reminderEntityPopup.setMethod("popup");
		ReminderEntity reminderEntityEmail = new ReminderEntity();
		reminderEntityEmail.setMinutes(10);
		reminderEntityEmail.setMethod("email");
		List<ReminderEntity> reminderEntityList = new ArrayList<ReminderEntity>();
		reminderEntityList.add(reminderEntityPopup);
		reminderEntityList.add(reminderEntityEmail);
		reminders = new ReminderEntityList(reminderEntityList, false);

	}

	public EventEntity(String id, String startDateTime, String endDateTime, String attendeesEmail, String description,
			Boolean guestsCanInviteOthers, Boolean guestsCanSeeOtherGuests, String location, String summary,
			String colorId, HashMap<String, String> extendedProperties) {
		this.id = id;
		if (summary != null) {
			this.summary = summary;
		}
		if (startDateTime != null) {
			DateEntity startDate = new DateEntity();
			startDate.setDateTime(startDateTime);
			this.startDate = startDate;
		}
		if (endDateTime != null) {
			DateEntity endDate = new DateEntity();
			endDate.setDateTime(endDateTime);
			this.endDate = endDate;
		}
		if (attendeesEmail != null) {
			for (String emailAttendee : attendeesEmail.split(",")) {
				attendees.add(new AttendeeEntity(emailAttendee.trim(), "accepted"));
			}
		}
		if (description != null) {
			this.description = description;
		}
		if (guestsCanInviteOthers != null) {
			this.guestsCanInviteOthers = guestsCanInviteOthers;
		}
		if (guestsCanSeeOtherGuests != null) {
			this.guestsCanSeeOtherGuests = guestsCanSeeOtherGuests;
		}
		if (location != null) {
			this.location = location;
		}
		if (summary != null) {
			this.summary = summary;
		}
		if (colorId != null) {
			this.colorId = colorId;
		}

		this.extendedProperties = new ExtendedPropertiesEntity();
		if (extendedProperties == null) {
			extendedProperties = new HashMap<String, String>();
		}
		extendedProperties.put("appId", "testProject");
		this.extendedProperties.setPrivateDataStore(extendedProperties);

	}

	public DateEntity getCreated() {
		return created;
	}

	public String getCreator() {
		return creator;
	}

	public String getEtag() {
		return etag;
	}

	public String getHtmlLink() {
		return htmlLink;
	}

	public String getICalUID() {
		return iCalUID;
	}

	public String getId() {
		return id;
	}

	public String getKind() {
		return kind;
	}

	public String getOrganizer() {
		return organizer;
	}

	public Integer getSequence() {
		return sequence;
	}

	public String getStatus() {
		return status;
	}

	public String getSummary() {
		return summary;
	}

	public DateEntity getUpdated() {
		return updated;
	}

	public String getLocation() {
		return location;
	}

	public List<AttendeeEntity> getAttendees() {
		return attendees;
	}

	public String getVisibility() {
		return visibility;
	}

	public String getTransparency() {
		return transparency;
	}

	public String getDescription() {
		return description;
	}

	public ExtendedPropertiesEntity getExtendedProperties() {
		return extendedProperties;
	}

	public String getColorId() {
		return colorId;
	}

	public Boolean getGuestsCanInviteOthers() {
		return guestsCanInviteOthers;
	}

	public Boolean getGuestsCanSeeOtherGuests() {
		return guestsCanSeeOtherGuests;
	}

	public Boolean getGuestsCanModify() {
		return guestsCanModify;
	}

	public java.util.List<String> getRecurrence() {
		return recurrence;
	}


	public DateEntity getStartDate() {
		return startDate;
	}

	public DateEntity getEndDate() {
		return endDate;
	}

	public DateEntity getOriginalStartTime() {
		return originalStartTime;
	}

	public String getHangoutLink() {
		return hangoutLink;
	}

}
