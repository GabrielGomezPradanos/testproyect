package com.calendar.sp.entities;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import com.google.gson.annotations.Expose;

/**
 * This class contains a list of NewEventEntity
 * 
 * @author anunez
 * 
 */
public class EventEntityList implements Serializable {

	protected static final Logger LOGGER = Logger.getLogger(EventEntityList.class.getName());
	private static final long serialVersionUID = -3614498036017991933L;
	public static final String FORMAT_DATE_UPDATEDATE_GOOGLE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String FORMAT_DATE_WITHOUT_TIME = "yyyy-MM-dd";
	public static TimeZone TZ_UTC = TimeZone.getTimeZone("UTC");

	@Expose
	private List<EventEntity> events;

	@Expose
	private String timeZone;

	@Expose
	private String updated;

	@Expose
	private String accessRole;

	@Expose
	private String nextPageToken;

	@Expose
	private String description;

	@Expose
	private String summary;

	@Expose
	private String calendarId;

	@Expose
	private int total;

	public EventEntityList() {
		events = new ArrayList<EventEntity>();
		total = 0;
	}

	public EventEntityList(long calendarLastUpdate, String calendarId, String fields) {
		this.calendarId = calendarId;
		if (fields.equalsIgnoreCase("") || fields.contains("updated")) {
			updated = EventEntityList.getDateEntity(new Date(calendarLastUpdate));
		}
		if (fields.equalsIgnoreCase("") || fields.contains("items")) {
			events = new ArrayList<EventEntity>();
		}
		total = 0;
	}

	public List<EventEntity> getEvents() {
		return events;
	}

	public void setEvents(List<EventEntity> events) {
		this.events = events;
	}

	public void addEvent(EventEntity event) {
		if (events == null) {
			events = new ArrayList<EventEntity>();
		}
		events.add(event);
	}

	public void addEvents(List<EventEntity> lEvents) {
		if (lEvents != null) {
			List<EventEntity> lResult = new ArrayList<EventEntity>();
			for (EventEntity eventEntity : lEvents) {
				lResult.add(eventEntity);
			}
			events = lResult;
		}
	}

	public void addAllEvents(List<EventEntity> lEvents) {
		if (events == null) {
			events = new ArrayList<EventEntity>();
		}
		if (lEvents != null) {
			List<EventEntity> lResult = new ArrayList<EventEntity>();
			for (EventEntity eventEntity : lEvents) {
				lResult.add(eventEntity);
			}
			events.addAll(lResult);
		}
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getAccessRole() {
		return accessRole;
	}

	public void setAccessRole(String accessRole) {
		this.accessRole = accessRole;
	}

	public String getNextPageToken() {
		return nextPageToken;
	}

	public void setNextPageToken(String nextPageToken) {
		this.nextPageToken = nextPageToken;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public static String getDateEntity(Date date) {
		return EventEntityList.getDateEntity(date, EventEntityList.FORMAT_DATE_UPDATEDATE_GOOGLE);
	}

	public static String getDateEntity(Date date, String format) {
		String time = null;
		if (date != null) {
			time = EventEntityList.getDateUTCString(date, format);
		}
		return time;
	}

	public static String getDateUTCString(Date dateToTransform, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setTimeZone(EventEntityList.TZ_UTC);
		return dateFormat.format(dateToTransform);
	}

	@Override
	public String toString() {
		return "EventEntityList [events=" + events + ", timeZone=" + timeZone + ", updated=" + updated
				+ ", accessRole=" + accessRole + ", nextPageToken=" + nextPageToken + ", description="
				+ description + ", summary=" + summary + ", calendarId=" + calendarId + ", total=" + total
				+ "]";
	}

}
