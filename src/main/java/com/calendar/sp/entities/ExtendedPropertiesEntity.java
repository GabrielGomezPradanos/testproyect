package com.calendar.sp.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;

import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.EmbedMap;

/**
 * This class contains the data of an event extended properties
 * 
 * @author anunez
 * 
 */
@Embed
public class ExtendedPropertiesEntity implements Serializable {
	private static final long serialVersionUID = 1506581164376456519L;
	/**
	 * Properties that are shared between copies of the event on other
	 * attendees' calendars. The value may be {@code null}.
	 */
	@EmbedMap
	private java.util.Map<String, String> shared;

	/**
	 * Properties that are private to the copy of the event that appears on this
	 * calendar. The value may be {@code null}.
	 */
	@EmbedMap
	private java.util.Map<String, String> calendarPrivate;

	public java.util.Map<String, String> getPrivate() {
		return calendarPrivate;
	}

	public void setPrivateDataStore(java.util.Map<String, String> calendarPrivate) {
		java.util.Map<String, String> myPrivateMap = new HashMap<String, String>();
		if (calendarPrivate != null) {
			for (Entry<String, String> entry : calendarPrivate.entrySet()) {
				String key = entry.getKey().replace(".", "^");
				myPrivateMap.put(key, entry.getValue());
			}
			this.calendarPrivate = myPrivateMap;
		}
	}

}
