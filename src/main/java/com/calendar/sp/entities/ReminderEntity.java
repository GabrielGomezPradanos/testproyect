package com.calendar.sp.entities;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Embed;

/**
 * This class contains the data of an event reminder
 * 
 * @author anunez
 * 
 */
@Embed
public class ReminderEntity implements Serializable {
	private static final long serialVersionUID = 2203950883493879826L;

	/**
	 * Number of minutes before the start of the event when the reminder should
	 * trigger. The value may be {@code null}.
	 */
	private Integer minutes;
	private String method;

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Integer getMinutes() {
		return minutes;
	}

	public String getMethod() {
		return method;
	}

}
