package com.calendar.sp.entities;

import java.io.Serializable;
import java.util.List;

import com.googlecode.objectify.annotation.Embed;

/**
 * This class contains a list of reminders and other useful data
 *
 * @author anunez
 *
 */
@Embed
public class ReminderEntityList implements Serializable {
	private static final long serialVersionUID = 7778064287776536576L;

	private List<ReminderEntity> overrides;
	private Boolean useDefault;

	public ReminderEntityList(List<ReminderEntity> overrides, Boolean useDefault) {
		super();
		this.overrides = overrides;
		this.useDefault = useDefault;
	}

	public List<ReminderEntity> getOverrides() {
		return overrides;
	}

	public void setOverrides(List<ReminderEntity> overrides) {
		this.overrides = overrides;
	}

	public Boolean getUseDefault() {
		return useDefault;
	}

	public void setUseDefault(Boolean useDefault) {
		this.useDefault = useDefault;
	}

}
