package com.calendar.sp.entities;

import java.io.Serializable;

public class ScopeEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4868664504017753322L;
	private String type;
	private String value;
	
	public ScopeEntity() {
	}
	public ScopeEntity(String type, String value) {
		this.type = type;
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
