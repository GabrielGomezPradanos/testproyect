package com.calendar.sp.entities;

public class Search {

	protected Integer maxAttendees;
	protected Integer maxResults;
	protected String pageToken;
	protected String q;
	protected Boolean showDeleted;
	protected Boolean showHiddenInvitations;
	protected Boolean singleEvents;
	protected String timeZone;
	protected String minAccessRole;
	protected boolean showHidden;
	protected String calendarId;
	protected String fields;
	protected Boolean alwaysIncludeEmail;
	protected String iCalUID;
	protected String orderBy;
	protected Integer startIndex;

	public Integer getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public Boolean getAlwaysIncludeEmail() {
		return alwaysIncludeEmail;
	}

	public void setAlwaysIncludeEmail(Boolean alwaysIncludeEmail) {
		this.alwaysIncludeEmail = alwaysIncludeEmail;
	}

	public String getiCalUID() {
		return iCalUID;
	}

	public void setiCalUID(String iCalUID) {
		this.iCalUID = iCalUID;
	}

	public Integer getMaxAttendees() {
		return maxAttendees;
	}

	public void setMaxAttendees(Integer maxAttendees) {
		this.maxAttendees = maxAttendees;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getPageToken() {
		return pageToken;
	}

	public void setPageToken(String pageToken) {
		this.pageToken = pageToken;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	public Boolean isShowDeleted() {
		return showDeleted;
	}

	public void setShowDeleted(Boolean showDeleted) {
		this.showDeleted = showDeleted;
	}

	public Boolean isShowHiddenInvitations() {
		return showHiddenInvitations;
	}

	public void setShowHiddenInvitations(Boolean showHiddenInvitations) {
		this.showHiddenInvitations = showHiddenInvitations;
	}

	public Boolean isSingleEvents() {
		return singleEvents;
	}

	public void setSingleEvents(Boolean singleEvents) {
		this.singleEvents = singleEvents;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getMinAccessRole() {
		return minAccessRole;
	}

	public void setMinAccessRole(String minAccessRole) {
		this.minAccessRole = minAccessRole;
	}

	public boolean isShowHidden() {
		return showHidden;
	}

	public void setShowHidden(boolean showHidden) {
		this.showHidden = showHidden;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

}
