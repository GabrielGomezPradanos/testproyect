package com.calendar.sp.entities;

import java.io.Serializable;



/**
 * This class contains all the parameters to make a search of events
 *
 * @author anunez
 *
 */
public class SearchOptionsEntity extends Search implements Serializable {

	private static final long serialVersionUID = -8251899991028363006L;

	private DateEntity timeMax;
	private DateEntity timeMin;
	private DateEntity updatedMin;

	public SearchOptionsEntity() {
		super();
	}

	public SearchOptionsEntity(String pageToken, DateEntity timeMax, DateEntity timeMin) {
		super();
		this.pageToken = pageToken;
		this.timeMax = timeMax;
		this.timeMin = timeMin;
	}

	public DateEntity getTimeMax() {
		return timeMax;
	}

	public void setTimeMax(DateEntity timeMax) {
		this.timeMax = timeMax;
	}

	public DateEntity getTimeMin() {
		return timeMin;
	}

	public void setTimeMin(DateEntity timeMin) {
		this.timeMin = timeMin;
	}

	public DateEntity getUpdatedMin() {
		return updatedMin;
	}

	public void setUpdatedMin(DateEntity updatedMin) {
		this.updatedMin = updatedMin;
	}

	@Override
	public String toString() {
		return "SearchOptionsEntity [ timeMax=" + timeMax + ", timeMin=" + timeMin + ",updatedMin="
				+ updatedMin + "]";
	}

}
