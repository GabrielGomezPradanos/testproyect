package com.calendar.sp.exceptions;

import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.calendar.sp.beans.ExceptionBean;
import com.google.gson.Gson;
import com.sun.jersey.api.Responses;

public class CalendarException extends WebApplicationException {

	private static final long serialVersionUID = 5150094724428022304L;
	protected static final Logger LOGGER = Logger.getLogger(CalendarException.class.getName());

	/**
	 * Creates a new instance.
	 * 
	 * @param message
	 *            The detail message.
	 * @param code
	 *            The error code.
	 */
	public CalendarException(String message, int code) {
		super(Response.status(code)
				.entity(new Gson().toJson(new ExceptionBean(String.valueOf(code), message)))
				.type(MediaType.APPLICATION_JSON).build());
		CalendarException.LOGGER.severe(message);
	}



}
