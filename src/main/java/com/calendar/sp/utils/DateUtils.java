package com.calendar.sp.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.calendar.sp.entities.DateEntity;
import com.calendar.sp.exceptions.CalendarException;

/**
 * This class contains useful methods to process dates
 * 
 * @author anunez
 * 
 */
public class DateUtils {

	private static final TimeZone GMT = TimeZone.getTimeZone("GMT");
	public static final String FORMAT_DATE_UPDATEDATE_GOOGLE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String FORMAT_DATE_WITHOUT_TIME = "yyyy-MM-dd";
	public static final TimeZone TZ_UTC = TimeZone.getTimeZone("UTC");

	/**
	 * Parse a RFC 3339 string to a DateEntity object
	 * 
	 * @param data
	 * @return
	 * @throws CalendarException
	 */
	public static DateEntity valueInRFCToDateEntity(String str) throws NumberFormatException {
		
			Calendar dateTime = new GregorianCalendar(DateUtils.GMT);
			int year = Integer.parseInt(str.substring(0, 4));
			int month = Integer.parseInt(str.substring(5, 7)) - 1;
			int day = Integer.parseInt(str.substring(8, 10));
			int tzIndex=0;
			int length = str.length();
				int hourOfDay = Integer.parseInt(str.substring(11, 13));
				int minute = Integer.parseInt(str.substring(14, 16));
				int second = Integer.parseInt(str.substring(17, 19));
				dateTime.set(year, month, day, hourOfDay, minute, second);
				if (length > 19) {
					if (str.charAt(19) == '.') {
						int milliseconds = Integer.parseInt(str.substring(20, 23));
						dateTime.set(Calendar.MILLISECOND, milliseconds);
						tzIndex = 23;
					} 
				} 

			Integer tzShiftInteger = 0;
			long value = dateTime.getTimeInMillis();
			int tzShift = 0;
			if (length > tzIndex) {
				if (Character.toUpperCase(str.charAt(tzIndex)) == 'Z') {
					tzShift = 0;
				}
				tzShiftInteger = tzShift;
			}
			DateEntity dateEntity = new DateEntity(value, tzShiftInteger);
			return dateEntity;
		

	}
}
