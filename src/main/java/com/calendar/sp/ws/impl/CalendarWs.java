package com.calendar.sp.ws.impl;

import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.calendar.sp.biz.impl.CalendarBizImpl;
import com.fga.exceptions.FGAException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

@Path("/calendar")
public class CalendarWs {

	protected static final Logger LOGGER = Logger.getLogger(CalendarWs.class.getName());
	public final static String CHARSET_UTF8 = ";charset=UTF-8";

	
	@SuppressWarnings("unchecked")
	@Path("/create")
	@POST
	@Produces({ MediaType.APPLICATION_JSON + CHARSET_UTF8 })
	public String createAppointment(@Context HttpServletRequest request, @Context HttpServletResponse response,
			@FormParam("calendarId") String calendarId, @FormParam("startDateTime") String startDateTime,
			@FormParam("endDateTime") String endDateTime, @FormParam("attendeesEmail") String attendeesEmail,
			@FormParam("description") String description,
			@FormParam("guestsCanInviteOthers") Boolean guestsCanInviteOthers,
			@FormParam("guestsCanSeeOtherGuests") Boolean guestsCanSeeOtherGuests,
			@FormParam("location") String location, @FormParam("summary") String summary,
			@FormParam("colorId") String colorId, @FormParam("extendedProperties") String extendedProperties,
			@FormParam("sendNotifications") Boolean sendNotifications) {

		return new GsonBuilder().disableHtmlEscaping().create().toJson(new CalendarBizImpl().createEvent(calendarId, startDateTime, endDateTime, attendeesEmail,
				description, guestsCanInviteOthers, guestsCanSeeOtherGuests, location, summary, colorId,
				new Gson().fromJson(extendedProperties, HashMap.class), sendNotifications, false, Boolean.FALSE));

	}

	@SuppressWarnings("unchecked")
	@Path("/update")
	@PUT
	@Produces({ MediaType.APPLICATION_JSON + CHARSET_UTF8 })
	public String updateAppointment(@Context HttpServletRequest request, @Context HttpServletResponse response,
			@FormParam("calendarId") String calendarId, @FormParam("startDateTime") String startDateTime,
			@FormParam("endDateTime") String endDateTime, @FormParam("attendeesEmail") String attendeesEmail,
			@FormParam("description") String description,
			@FormParam("guestsCanInviteOthers") Boolean guestsCanInviteOthers,
			@FormParam("guestsCanSeeOtherGuests") Boolean guestsCanSeeOtherGuests,
			@FormParam("location") String location, @FormParam("summary") String summary,
			@FormParam("colorId") String colorId, @FormParam("eventId") String eventId,
			@FormParam("extendedProperties") String extendedProperties,
			@FormParam("sendNotifications") Boolean sendNotifications) throws JsonSyntaxException, FGAException {

		CalendarWs.LOGGER.info("Mapa: " + extendedProperties);
		CalendarWs.LOGGER.info("Campo description: " + description + "para el evento: " + eventId + "en la modificacion"
				+ "y usuario: " + calendarId);
		return new GsonBuilder().disableHtmlEscaping().create().toJson(new CalendarBizImpl().updateEvent(calendarId, startDateTime, endDateTime,
				attendeesEmail, description, guestsCanInviteOthers, guestsCanSeeOtherGuests, location, summary, colorId,
				eventId, new Gson().fromJson(extendedProperties, HashMap.class), sendNotifications, Boolean.FALSE));
	}
}
