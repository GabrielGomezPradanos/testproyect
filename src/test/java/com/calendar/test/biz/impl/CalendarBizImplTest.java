package com.calendar.test.biz.impl;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.junit.Test;

import com.calendar.sp.beans.ExceptionBean;
import com.calendar.sp.biz.impl.CalendarBizImpl;
import com.calendar.sp.entities.EventEntity;
import com.calendar.sp.exceptions.CalendarException;
import com.calendar.sp.utils.DateUtils;
import com.google.gson.Gson;


public class CalendarBizImplTest {

	@Test
	public void testCalendarCreateIdNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR, exceptionBean.getMessage());
			
		}
	}

	@Test
	public void testCalendarCreateIdEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("", null, null, null, null, null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateStartDateTimeNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("gabriel.gomez.contractor@bbva.com", null, null, null, null, null, null, null, null,
					null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateStartDateTimeEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("gabriel.gomez.contractor@bbva.com", "", null, null, null, null, null, null, null,
					null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateEndDateTimeNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z", null, null, null,
					null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateEndDateTimeEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z", "", null, null, null,
					null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateAttendeesNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z",
					"2016-03-29T04:41:06.000Z", null, null, null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateAttendeesEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.createEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z",
					"2016-03-29T04:41:06.000Z", "", null, null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarCreateEvent() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			EventEntity event = calendar.createEvent("gabriel.gomez.contractor@dev.bbva.com",
					"2016-04-07T20:00:00.000Z", "2016-04-07T20:30:00.000Z", "gabriel.gomez.contractor@dev.bbva.com",
					"description", Boolean.FALSE, Boolean.FALSE, "location", "sumary", "1", null, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
			System.out.println("Entity: " + new Gson().toJson(event));
			assertEquals(event.getStartDate().getValue(),
					DateUtils.valueInRFCToDateEntity("2016-04-07T20:00:00.000Z").getValue());
			assertEquals(event.getEndDate().getValue(),
					DateUtils.valueInRFCToDateEntity("2016-04-07T20:30:00.000Z").getValue());
			assertEquals(event.getAttendees().size(), 1);
			assertEquals(event.getAttendees().get(0).getEmail(), "gabriel.gomez.contractor@dev.bbva.com");
			assertEquals(event.getColorId(), "1");
			assertEquals(event.getCreated(), null);
			assertEquals(event.getCreator(), null);
			assertEquals(event.getDescription(), "description");
			assertEquals(event.getEtag(), null);
			assertEquals(
					((HashMap<String, String>) event.getExtendedProperties().getPrivate()).keySet().contains("appId"),
					Boolean.TRUE);
			assertEquals(((HashMap<String, String>) event.getExtendedProperties().getPrivate()).values()
					.contains("testProject"), Boolean.TRUE);
			assertEquals(event.getGuestsCanInviteOthers(), Boolean.FALSE);
			assertEquals(event.getGuestsCanModify(), null);
			assertEquals(event.getGuestsCanSeeOtherGuests(), Boolean.FALSE);
			assertEquals(event.getHangoutLink(), null);
			assertEquals(event.getHtmlLink(), null);
			assertEquals(event.getICalUID(), null);
			assertEquals(event.getId(), null);
			assertEquals(event.getKind(), null);
			assertEquals(event.getLocation(), "location");
			assertEquals(event.getOrganizer(), null);
			assertEquals(event.getOriginalStartTime(), null);
			assertEquals(event.getRecurrence(), null);
			assertEquals(event.getSequence(), null);
			assertEquals(event.getStatus(), null);
			assertEquals(event.getSummary(), "sumary");
			assertEquals(event.getTransparency(), null);
			assertEquals(event.getUpdated(), null);
			assertEquals(event.getVisibility(), null);

		} catch (CalendarException e) {
		}
	}

	@Test
	public void testCalendarUpdateIdNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent(null, null, null, null, null, null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateIdEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("", null, null, null, null, null, null, null, null, null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.CALENDAR_ID_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateEventIdTimeNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", null, null, null, null, null, null, null, null,
					null, null, null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.EVENT_ID_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateEventIdTimeEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", null, null, null, null, null, null, null, null,
					null, "", null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.EVENT_ID_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateStartDateTimeNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", null, null, null, null, null, null, null, null,
					null, "fjdsañkljfañsld", null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateStartDateTimeEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", "", null, null, null, null, null, null, null,
					null, "fjdsañkljfañsld", null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.START_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateEndDateTimeNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z", null, null, null,
					null, null, null, null, null, "fjdsañkljfañsld", null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateEndDateTimeEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z", "", null, null, null,
					null, null, null, null, "fjdsañkljfañsld", null, null, null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.END_DATE_TIME_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateAttendeesNullException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z",
					"2016-03-29T04:41:06.000Z", "gabriel.gomez.contractor@bbva.com", "description", Boolean.FALSE, Boolean.FALSE, "location","summary", "2", "fjdsañkljfañsld", null, Boolean.FALSE,
					Boolean.FALSE);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateAttendeesEmptyException() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			calendar.updateEvent("gabriel.gomez.contractor@bbva.com", "2016-03-29T04:41:06.000Z",
					"2016-03-29T04:41:06.000Z", "", null, null, null, null, null, null, "fjdsañkljfañsld", null, null,
					null);
		} catch (CalendarException e) {
			ExceptionBean exceptionBean = new Gson().fromJson(e.getResponse().getEntity().toString(),
					ExceptionBean.class);
			System.out.println(exceptionBean.getCode());
			System.out.println(exceptionBean.getMessage());
			assertEquals(CalendarBizImpl.ERROR_CONSTANT, Integer.valueOf(exceptionBean.getCode()).intValue());
			assertEquals(CalendarBizImpl.ATTENDEES_EMAIL_EMPTY_ERROR, exceptionBean.getMessage());
		}
	}

	@Test
	public void testCalendarUpdateEvent() {
		CalendarBizImpl calendar = new CalendarBizImpl();
		try {
			EventEntity event = calendar.updateEvent("gabriel.gomez.contractor@dev.bbva.com",
					"2016-04-07T20:00:00.000Z", "2016-04-07T20:30:00.000Z", "gabriel.gomez.contractor@dev.bbva.com",
					"description", Boolean.FALSE, Boolean.FALSE, "location", "sumary", "1", "fjdsañkljfañsld", null, Boolean.FALSE, Boolean.FALSE);
			System.out.println("Entity: " + new Gson().toJson(event));
			assertEquals(event.getStartDate().getValue(),
					DateUtils.valueInRFCToDateEntity("2016-04-07T20:00:00.000Z").getValue());
			assertEquals(event.getEndDate().getValue(),
					DateUtils.valueInRFCToDateEntity("2016-04-07T20:30:00.000Z").getValue());
			assertEquals(event.getAttendees().size(), 1);
			assertEquals(event.getAttendees().get(0).getEmail(), "gabriel.gomez.contractor@dev.bbva.com");
			assertEquals(event.getColorId(), "1");
			assertEquals(event.getCreated(), null);
			assertEquals(event.getCreator(), null);
			assertEquals(event.getDescription(), "description");
			assertEquals(event.getEtag(), null);
			assertEquals(
					((HashMap<String, String>) event.getExtendedProperties().getPrivate()).keySet().contains("appId"),
					Boolean.TRUE);
			assertEquals(((HashMap<String, String>) event.getExtendedProperties().getPrivate()).values()
					.contains("testProject"), Boolean.TRUE);
			assertEquals(event.getGuestsCanInviteOthers(), Boolean.FALSE);
			assertEquals(event.getGuestsCanModify(), null);
			assertEquals(event.getGuestsCanSeeOtherGuests(), Boolean.FALSE);
			assertEquals(event.getHangoutLink(), null);
			assertEquals(event.getHtmlLink(), null);
			assertEquals(event.getICalUID(), null);
			assertEquals(event.getId(), "fjdsañkljfañsld");
			assertEquals(event.getKind(), null);
			assertEquals(event.getLocation(), "location");
			assertEquals(event.getOrganizer(), null);
			assertEquals(event.getOriginalStartTime(), null);
			assertEquals(event.getRecurrence(), null);
			assertEquals(event.getSequence(), null);
			assertEquals(event.getStatus(), null);
			assertEquals(event.getSummary(), "sumary");
			assertEquals(event.getTransparency(), null);
			assertEquals(event.getUpdated(), null);
			assertEquals(event.getVisibility(), null);

		} catch (CalendarException e) {
		}
	}

}
